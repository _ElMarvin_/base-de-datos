package app;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.DriverManager;

public class Database {

    //Conectarse a la base de datos
    Connection miConexion;
    PreparedStatement clausula;

    boolean conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String cadena="jdbc:mysql://localhost:3306/cplant?autoReconnect=true&useSSL=false";
            String usuario="root";
            String clave="mysql2019";

            miConexion = (Connection) DriverManager.getConnection(cadena, usuario, clave);
            System.out.println("Nos conectamos");
        } catch (Exception e) {
            System.out.print(e);
        }
        return true;
    }
///Crear una clausula SQL para escribir en la base de datos.
    boolean escribir(String nombre, String planta, int numero) {

        /*String Nombre = "Probando la vida";
        int Telefono = 9856247;
        String Planta = "Mariajuana";
        */
        try {

            String insertar = "INSERT INTO `cplant`.`usuario` (`Nombre`,`Telefono`,`Planta`)VALUES(?,?,?)";
            clausula = (PreparedStatement) miConexion.prepareStatement(insertar);
            clausula.setString(1, nombre);
            clausula.setInt(2, numero);
            clausula.setString(3, planta);
            clausula.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }

        return true;
    }
}
